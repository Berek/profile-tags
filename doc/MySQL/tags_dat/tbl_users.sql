-- 会员信息表： tbl_users
-- 电商网站中用户基本信息表，总共38个字段，除去主键ID外共37个字段信息。
-- 共有950条数据

CREATE TABLE `tbl_users` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`siteId` int(10) unsigned NOT NULL,
`avatarImageFileId` varchar(255) DEFAULT NULL,
`email` varchar(120) DEFAULT NULL,
`username` varchar(60) NOT NULL COMMENT '用户名',
`password` varchar(32) DEFAULT NULL COMMENT '密码',
`salt` varchar(10) DEFAULT NULL COMMENT '扰码',
`registerTime` int(10) unsigned NOT NULL COMMENT '注册时间',
`lastLoginTime` int(10) unsigned DEFAULT NULL COMMENT '最后登录时间',
`lastLoginIp` varchar(15) DEFAULT NULL COMMENT '最后登录ip',
`memberRankId` int(10) unsigned DEFAULT NULL COMMENT '特殊会员等级id，0表示
非特殊会员等级',
`bigCustomerId` int(10) unsigned DEFAULT NULL COMMENT '所属的大客户ID',
`lastAddressId` int(10) unsigned DEFAULT NULL COMMENT '上次使用的收货地址',
`lastPaymentCode` varchar(20) DEFAULT NULL COMMENT '上次使用的支付方式',
`gender` tinyint(3) unsigned DEFAULT NULL COMMENT '性别：0保密1男2女',
`birthday` date DEFAULT NULL COMMENT '生日',
`qq` varchar(20) DEFAULT NULL,
`job` varchar(60) DEFAULT NULL COMMENT '职业；1学生、2公务员、3军人、4警察、5教师、6白领',
`mobile` varchar(15) DEFAULT NULL COMMENT '手机',
`politicalFace` int(1) unsigned DEFAULT NULL COMMENT '政治面貌：1群众、2党员、3无党派人士',
`nationality` varchar(20) DEFAULT NULL COMMENT '国籍：1中国大陆、2美国、3英国、4日本、5其他',
`validateCode` varchar(32) DEFAULT NULL COMMENT '找回密码时的验证code',
`pwdErrCount` tinyint(3) DEFAULT NULL COMMENT '密码输入错误次数',
`source` varchar(20) DEFAULT NULL COMMENT '会员来源',
`marriage` varchar(60) DEFAULT NULL COMMENT '婚姻状况：1未婚、2已婚、3离异',
`money` decimal(15,2) DEFAULT NULL COMMENT '账户余额',
`moneyPwd` varchar(32) DEFAULT NULL COMMENT '余额支付密码',
`isEmailVerify` tinyint(1) DEFAULT NULL COMMENT '是否验证email',
`isSmsVerify` tinyint(1) DEFAULT NULL COMMENT '是否验证短信',
`smsVerifyCode` varchar(30) DEFAULT NULL COMMENT '邮件验证码',
`emailVerifyCode` varchar(30) DEFAULT NULL COMMENT '短信验证码',
`verifySendCoupon` tinyint(1) DEFAULT NULL COMMENT '是否验证发送优惠券',
`canReceiveEmail` tinyint(1) DEFAULT NULL COMMENT '是否接收邮件',
`modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE
CURRENT_TIMESTAMP COMMENT '最后更新时间',
`channelId` tinyint(4) DEFAULT '0' COMMENT '??EP???',
`grade_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '等级ID',
`nick_name` varchar(60) NOT NULL DEFAULT '' COMMENT '昵称',
`is_blackList` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否黑名单 : 0:非黑名单 1：黑名单',
PRIMARY KEY (`id`),
KEY `siteId` (`siteId`,`email`),
KEY `memberRankId` (`memberRankId`)
) ENGINE=InnoDB AUTO_INCREMENT=951 DEFAULT CHARSET=utf8;